import { load } from "cheerio";
import * as fs from "fs";
import StealthPlugin from "puppeteer-extra-plugin-stealth";
import UserAgent from "user-agents";
import puppeteer from "puppeteer-extra";
import csv from "csv-parser";

const FORTNITE_BASE_URL = "https://www.fortnite.com";

const excluded_categories = ["by epic"];
const excluded_owners = ["@thelegogroup"];

puppeteer.use(StealthPlugin());

async function getCookies(url) {
  const browserObj = await puppeteer.launch();
  const page = await browserObj.newPage();

  const userAgent = new UserAgent();
  await page.setUserAgent(userAgent.toString());

  try {
    await page.goto(url);
    const cookies = await page.cookies();

    return cookies;
  } catch (error) {
    console.error("Error:", error);
  } finally {
    await browserObj.close();
  }
}

async function scrapeWebsite(url, cookies) {
  var foundSecurityCheck = false;
  var max_retries = 7;
  var securityCheckString = "Please complete a security check to continue";
  do {
    const browserObj = await puppeteer.launch({
      timeout: 100000,
      headless: true,
    });
    const page = await browserObj.newPage();

    const userAgent = new UserAgent();
    await page.setUserAgent(userAgent.toString());

    for (const cookie of cookies) {
      await page.setCookie(cookie);
    }

    try {
      await page.goto(url);
      await page.waitForSelector("body");

      // get the HTML content of the entire webpage
      const htmlContent = await page.content();

      const pageUrl = page.url();

      // check if the page has a security check
      foundSecurityCheck = await page.evaluate((securityCheckString) => {
        return document.body.innerText.includes(securityCheckString);
      }, securityCheckString);

      if (foundSecurityCheck && max_retries > 0) {
        console.log("Security check has been found, retry to fetch the page");
        max_retries--;
        await new Promise((resolve) => setTimeout(resolve, 5000));
      } else {
        return [htmlContent, pageUrl];
      }
    } catch (error) {
      console.error("Error:", error);
      return;
    } finally {
      // close the browser
      await browserObj.close();
    }
  } while (foundSecurityCheck);
}

function getPrevoiuslyFetchedGames(gamesFilename) {
  return new Promise((resolve, reject) => {
    fs.access(gamesFilename, fs.constants.F_OK, (err) => {
      if (err) {
        console.error("Error: File does not exist or is not readable.");
        reject(err);
        return;
      }

      var data = [];
      const stream = fs.createReadStream(gamesFilename);

      stream.on("error", (err) => {
        console.error("Error reading file:", err);
        reject(err);
      });

      stream
        .pipe(csv())
        .on("data", (row) => {
          data.push(row);
        })
        .on("end", () => {
          resolve(data); // Resolve the promise with the data once parsing is complete
        });
    });
  });
}

function isDuplicatedGame(games, gameName) {
  if (!games) return false;
  for (const game of games) {
    if (game.Name == gameName) {
      return true;
    }
  }
  return false;
}

async function extractLinks() {
  // read HTML file
  const fortniteHtmlSourceCodeFilename = "template.html";
  const outputFilename = "output.csv";

  const htmlContent = fs.readFileSync(fortniteHtmlSourceCodeFilename, "utf8");

  var prevoiuslyFetchedGames = [];
  await getPrevoiuslyFetchedGames(outputFilename)
    .then((res) => {
      prevoiuslyFetchedGames = res;
    })
    .catch((err) => {
      console.log(err);
    });

  const fortniteCookies = await getCookies(FORTNITE_BASE_URL);

  // load HTML content into Cheerio
  const $ = load(htmlContent);

  // find the script tag containing window.__remixContext
  const scriptContent = $('script:contains("window.__remixContext")').html();

  const prefix = "window.__remixContext =";

  // exclude last character ";"
  const data = JSON.parse(
    scriptContent.trim().substring(prefix.length, scriptContent.length - 1)
  );

  const islandsCategories = data.state.loaderData["routes/_index"].islands;

  const csvColumns = ["Name", "CCU", "Link", "Discord", "Twitter"];
  var csvData = [];

  var islandsMap = new Map();

  var cnt = 10 ;

  for (const category of islandsCategories) {
    if (cnt <=0) break;
    const categoryLabel = category.category.label.toLowerCase();
    if (excluded_categories.includes(categoryLabel)) {
      continue;
    }

    for (const island of category.islands) {
      if (cnt <=0) break;

      try {
        if (isDuplicatedGame(prevoiuslyFetchedGames, island.title)) {
          console.log(
            "found prevoiusly fetched island : " +
              island.title +
              " (Skipping ...)"
          );
          continue;
        }

        if (islandsMap.has(island.title)) {
          console.log(
            "found duplicated island : " + island.title + " (Skipping ...)"
          );
          continue;
        }
        cnt --;
        console.log("Getting info for " + island.title);

        islandsMap.set(island.title, true);

        var csvRow = [
          island.title,
          island.ccu,
          FORTNITE_BASE_URL + island.islandUrl,
        ];

        var [pageContent, pageUrl] = await scrapeWebsite(
          FORTNITE_BASE_URL + island.islandUrl,
          fortniteCookies
        );

        var foundExcludedOwner = false;
        for (const exc_owner of excluded_owners) {
          if (pageUrl.includes(exc_owner)) {
            console.log(
              "found an island with excluded owner : " +
                exc_owner +
                " (Skipping ...)"
            );
            foundExcludedOwner = true;
            break;
          }
        }

        if (foundExcludedOwner) {
          continue;
        }

        var islandPage = load(pageContent);

        const discordLinks = islandPage("a[href*=discord]");
        if (discordLinks.length > 0) {
          const discordLink = islandPage(discordLinks[0]).attr("href");
          if (discordLink && discordLink !== "") {
            csvRow.push(discordLink);
          }
        }

        const twitterLinks = islandPage("a[href*=twitter]");
        if (twitterLinks.length > 0) {
          const twitterLink = islandPage(twitterLinks[0]).attr("href");
          if (twitterLink && twitterLink !== "") {
            csvRow.push(twitterLink);
          }
        }

        csvData.push(csvRow);

        // await 5 seconds
        await new Promise((resolve) => setTimeout(resolve, 5000));
      } catch (e) {
        console.error("Error:", e);
      }
    }
  }

  if (prevoiuslyFetchedGames.length > 0) {
    csvData = [csvColumns,...csvData];
  }

  const csvContent = csvData.map((row) => row.join(",")).join("\n");
  fs.appendFile(outputFilename, csvContent, (err) => {
    if (err) {
      console.error("Error writing CSV file:", err);
    } else {
      console.log("CSV file created successfully!");
    }
  });
}

await extractLinks();
